// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


let menuSection = document.querySelector('.tabs');

let menuContentArray = [...document.querySelectorAll('.tabs-content li')];
for (i = 1 ; i < menuContentArray.length; i++){
    menuContentArray[i].style.display = 'none'
}

let menuTabsArray = [...document.getElementsByClassName('tabs-title')];

menuSection.addEventListener('click', (event) => {
    let currentActive = document.querySelector('.active');
    if (currentActive !== event.target){
        currentActive.classList.remove('active')
        event.target.classList.add('active')
        let index = menuTabsArray.indexOf(event.target);
        let previousIndex = menuTabsArray.indexOf(currentActive);
        menuContentArray[previousIndex].style.display = 'none'
        menuContentArray[index].style.display = 'block'
    }
})
