// Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
// Через 3 секунды вместо нее должна быть показана вторая картинка.
// Еще через 3 секунды - третья.
// Еще через 3 секунды - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

let imgArray = document.querySelectorAll('.images-wrapper img');
for (item of imgArray){
    item.style.display = 'none'
}
let count = 0;
let Interval;
document.addEventListener('click',(event) => {
    if (event.target === document.getElementsByClassName('stop-just__stop')[0]){
    clearInterval(Interval)
    }
    if (event.target === document.getElementsByClassName('go-btn')[0]){
        imagePopInterval()
    }
})
let imagePopInterval = function ourInterval(){
    Interval = setInterval(() => {
    if (count < 4){
        imgArray[count].style.removeProperty('display')
        count++
    }
    else{
        for (item of imgArray){
            item.style.display = 'none'
        }
        count = 0
    }

}, 1000);
}
imagePopInterval()