// Добавить в домашнее задание HTML/CSS №4 (Flex/Grid) различные эффекты с использованием jQuery
// Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
// При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
// Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
// Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции.

$(".hero-title").before(`<a name="THE LATEST NEWS ON DESIGN & ARCHITECTURE"> </a>`)
$(".posts-heading").before(`<a name="MOST POPULAR POSTS"> </a>`)
$(".clients-heading").before(`<a name="OUR MOST POPULAR CLIENTS"> </a>`)
$(".top-rated").before(`<a name="Top rated"> </a>`)
$(".hot-news").before(`<a name="Hot News"> </a>`)
$(".copyright").before(`<a name="footer"> </a>`)
$("body").prepend(`<a name="toTop"> </a>`)
document.addEventListener("scroll", function(){
    if(window.scrollY >= window.screen.height){
   $(".btn").css("display","block")
    }
    if(window.scrollY < window.screen.height){
        $(".btn").css("display","none")
    }
})
$(".btn").append(`<a name=""> </a>`)
$(".slideToggle").click(function(){
    $(".posts").slideToggle()
})
