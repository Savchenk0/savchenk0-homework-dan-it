// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы
let navigationColor = document.querySelector('.nav-menu');
navigationColor.style.background = localStorage.getItem('navigationColor.style.background')
let logoColor = document.querySelector('.skyblue-text');
logoColor.style.color = localStorage.getItem('logoColor.style.color')
let basementColor = document.querySelector('.basement-nav');
basementColor.style.background = localStorage.getItem('basementColor.style.background')
let changeThemeBtnColor = document.querySelector('.change-theme');
changeThemeBtnColor.style.background = localStorage.getItem('changeThemeBtnColor.style.background')
let  copyrightColor = document.querySelector('.copyright');
copyrightColor.style.background = localStorage.getItem('copyrightColor.style.background')
let count = 1;
document.addEventListener('click', (event) => {
    if (logoColor.style.color === 'brown'){
    if (event.target === changeThemeBtnColor ) {
        localStorage.clear()
        logoColor.style.color = 'skyblue';
        navigationColor.style.background = '#35444F';
        basementColor.style.background = 'rgba(99, 105, 110, 0.48)';
        changeThemeBtnColor.style.background = 'blue';
        copyrightColor.style.background = 'rgba(99, 105, 110, 0.48';
    }  
}
    else{
     if (event.target === changeThemeBtnColor && count%2 !== 0){
        logoColor.style.color = 'brown';
        localStorage.setItem('logoColor.style.color',logoColor.style.color)
        navigationColor.style.background = 'orange';
        localStorage.setItem('navigationColor.style.background',navigationColor.style.background)
        basementColor.style.background = 'yellow';
        localStorage.setItem('basementColor.style.background',basementColor.style.background)
        changeThemeBtnColor.style.background = 'purple';
        localStorage.setItem('changeThemeBtnColor.style.background',changeThemeBtnColor.style.background)
        copyrightColor.style.background = 'yellow';
        localStorage.setItem('copyrightColor.style.background',copyrightColor.style.background)
    }
}
    count++
})
