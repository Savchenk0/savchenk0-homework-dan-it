// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.


// В папке img лежат примеры реализации поля ввода и создающегося span.

function showInput() {
	let someInput = document.createElement('input');
	someInput.style.border = '1px solid black';
	someInput.setAttribute('placeholder', 'Price');
	someInput.type = 'number'
	let value;
	let newSpan;
	let crossBtn;
	let container;
	document.body.append(someInput)
	someInput.onfocus = function () {
		if (someInput.style.border === '4px solid red'){
			newSpan.remove()
		}
		someInput.style.border = '4px solid green'
	}
	someInput.onblur = function (){
		value = someInput.value
		if (value < 0){
		someInput.style.border = '4px solid red'
		newSpan = document.createElement('span');
		newSpan.innerText = `Please enter correct price`;
		newSpan.style.display = 'block'
		someInput.after(newSpan);
		}
		if (value >= 0 && value != ''){
		someInput.style.color = "green";
		someInput.style.border = '1px solid black';
		newSpan = document.createElement('span');
		crossBtn = document.createElement('button');
		crossBtn.textContent = '\u274C'
		container = document.createElement('div');
		newSpan.innerText = `Текущая цена: ${value}`;
		someInput.before(container)
		container.append(newSpan);
		container.append(crossBtn);
	}
	}
}
document.addEventListener('DOMContentLoaded', () => {
	showInput()
})

