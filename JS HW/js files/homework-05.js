// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser(name,surname){
    let birthdate = prompt('Введите дату рождения','24.08.2001');
    let newUser = {
        birthday: birthdate,
        firstName: name,
        lastName: surname,
        getLogin(){
            return (this.firstName[1] + this.lastName).toLowerCase()
        },
        getAge(){
            let normalizedBirthday = this.birthday.split('.').reverse().join('.')     
            let ageDate = new Date(Date.now() - new Date(normalizedBirthday))
            let result = ageDate.getUTCFullYear() - 1970
            if (result < 0) return 0
            return result
    },
        getPassword(){
            let normalizedBirthday = this.birthday.split('.').reverse().join('.')   
            let ageDate = new Date(normalizedBirthday)
            return this.firstName[1].toUpperCase() + this.lastName.toLowerCase() + ageDate.getFullYear()
        }
    }
    return newUser
}
let b = createNewUser('Sasha','Savchenko')
console.log(b,b.getLogin())
console.log(b.getAge())
console.log(b.getPassword())