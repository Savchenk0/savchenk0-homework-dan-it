// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
// Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
// Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


// После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

let name,
     age
do {
    name = prompt('Enter your name')
    age = +prompt('Enter your age')
}
while(
    name === null || name === "" || !isNaN(name) ||   age === null || age === "" || isNaN(age) 
)

if (age < 18){
    alert(' You are not allowed to visit this website.')
}
else if(age >= 18 && age <= 22 ){
    let approve =  confirm(' Are you sure you want to continue? ')
    if (approve){
        alert(`Welcome, ${name}`)
    }
    else{
    alert(' You are not allowed to visit this website.')

    }
}
else if (age > 22){
    alert(`Welcome, ${name}`)

}