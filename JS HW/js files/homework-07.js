// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//  интерпретация хтмл документа, через последовательную иерархию объектов

// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;

// Примеры массивов, которые можно выводить на экран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];

// Можно взять любой другой массив.
 let toBuyList = ["помидоры", "огурцы", "молоко 2 л.", "яйца 2 десятка", "бублики", "подсолнечное масло"]
function getListFromArray(array,parent = document.body){
    let resultList = document.createElement('ul'),
    listElementsArray = array.map(elem => '<li>' + elem + '</li>');
    for (item of listElementsArray){
        resultList.insertAdjacentHTML('beforeend', item)
    }
    return parent.append(resultList)
}
getListFromArray(toBuyList)